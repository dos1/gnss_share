# Copyright(c) 2021 by craftyguy "Clayton Craft" <clayton@craftyguy.net>
# Distributed under GPLv3+ (see COPYING) WITHOUT ANY WARRANTY.
import sys

if __name__ == '__main__':
    from gnss_share import main
    sys.exit(main.main("dev"))
